'use strict'
const mongoose = require('mongoose')
module.exports={
    firstName:{
        type:String,
        lowercase:String,
        trim:true
    },
    lastName:{
        type:String,
        lowercase:String,
        trim:true
    },
    email:{
        type:String,
        lowercase:String,
        trim:true
    },
    phone:String,
    qualification:String,
    college:String,
    post:{
        type:mongoose.Schema.ObjectId,
        ref:'post'
    }

}