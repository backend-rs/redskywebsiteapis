'use strict'

const mongoose = require('mongoose')

module.exports={
    postTitle:{
        type:String,
        lowercase:true,
    },
    postType:{
        type:String,
        default:'job',
        enum:['job','blogs']
    },
    profile:{
        type:String,
        default:'Software Developer',
        enum:['Software Developer','Testers','Software Architects','Business Analysts','Project Managers','Graphic designers']
    },
    Responsibilities:{
        type:String,
        lowercase:true,
        trim:true
    },
    technologies:{
        type:String,
        lowercase:true,
        trim:true
    },
    requirement:{
        type:String,
        lowercase:true,
        trim:true
    },
    skills:{
        type:String,
        lowercase:true,
        trim:true
    },
    experience:{
        type:String,
        lowercase:true,
        trim:true
    },
    jobType:{
        type:String,
        default:'Full-time',
        enum: ['Full-time', 'Fresher','Part-time','Internship']
    },
    salary:{
        type:String,
        lowercase:true,
        trim:true
    },
    schedule:{
        type:String,
        lowercase:true,
        trim:true
    },
    education:{
        type:String,
        lowercase:true,
        trim:true
    },
    image: {
        url: String,
        thumbnail: String,
        resize_url: String,
        resize_thumbnail: String
    },
    WorkRemotely:{
        type:String,
        lowercase:true,
        trim:true
    },
    workspace:{
        type: mongoose.Schema.ObjectId,
        ref: 'workspace'
    },
    createdAt:{
        type:Date,
        
    },
    updatedAt:{
        type:Date,
      
    },
    updatedby:{
        type:Date,
    }
}