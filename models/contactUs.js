'use strict'

module.exports={
    name:{
        type:String,
        lowercase:true,
        trim:true
    },
    email:{
        type:String,
        lowercase:true,
        trim:true
    },
    phone:String,
    queryFor:String,
    message:String
}