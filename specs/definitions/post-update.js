module.exports = {
    profile: 'string',
    technologies: 'string',
    requirement: 'string',
    skills: 'string',
    experience: 'string',
    jobType:'string',
    salary:'string',
    schedule:'string',
    education:'string'
}