module.exports = {
    firstName: 'string',
    lastName: 'string',
    email:'string',
    phone: 'string',
    qualification: 'string',
    college: 'string',
    post:'string'
}