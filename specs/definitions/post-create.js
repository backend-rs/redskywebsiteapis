module.exports = {
    postTitle:"string",
    postType:"string",
    profile: 'string',
    Responsibilities:'string',
    technologies: 'string',
    requirement: 'string',
    skills: 'string',
    experience: 'string',
    jobType:'string',
    salary:'string',
    schedule:'string',
    education:'string',
    image: {
        url: 'string',
        thumbnail: 'string',
        resize_url: 'string',
        resize_thumbnail: 'string'
    },
    WorkRemotely:'string',
    workspace:'string'
}