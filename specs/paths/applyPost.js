module.exports = [{
    url: '/',
    post: {
        summary: 'Create',
        description: 'Create apply details',
        parameters: [{
            in: 'body',
            name: 'body',
            description: 'Model of applyPost detail creation',
            required: true,
            schema: {
                $ref: '#/definitions/applyPostReq'
            }
        }],
        responses: {
            default: {
                description: 'Unexpected error',
                schema: {
                    $ref: '#/definitions/Error'
                }
            }
        }
    },
}
]