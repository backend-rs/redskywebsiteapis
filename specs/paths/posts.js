module.exports = [{
    url: '/',
    post: {
        summary: 'Create',
        description: 'Create post details',
        parameters: [{
            in: 'body',
            name: 'body',
            description: 'Model of post detail creation',
            required: true,
            schema: {
                $ref: '#/definitions/postCreateReq'
            }
        }],
        responses: {
            default: {
                description: 'Unexpected error',
                schema: {
                    $ref: '#/definitions/Error'
                }
            }
        }
    },
    get: {
        summary: 'Search',
        description: 'get post lists',
        parameters: [
            {
                in: 'query',
                name: 'pageNo',
                description: 'get  post',
                required: false,
                type: 'number'
            },
            {
                in: 'query',
                name: 'items',
                description: 'get  post',
                required: false,
                type: 'number'
            },


        ],
        responses: {
            default: {
                description: 'Unexpected error',
                schema: {
                    $ref: '#/definitions/Error'
                }
            }
        }
    },
    
},
{
    url: '/{id}',
    get: {
        summary: 'Get',
        description: 'get post by Id',
        parameters: [{
            //     in: 'header',
            //     name: 'x-access-token',
            //     description: 'token to access api',
            //     required: true,
            //     type: 'string'
            // },
            // {
            in: 'path',
            name: 'id',
            description: 'fileId',
            required: true,
            type: 'string'
        }
        ],
        responses: {
            default: {
                description: 'Unexpected error',
                schema: {
                    $ref: '#/definitions/Error'
                }
            }
        }
    }
},
{
    url: '/delete/{id}',
    delete: {
        summary: 'delete',
        description: ' delete by Id',
        parameters: [
            {
                in: 'path',
                name: 'id',
                description: 'fileId',
                required: true,
                type: 'string'
            }
        ],
        responses: {
            default: {
                description: 'Unexpected error',
                schema: {
                    $ref: '#/definitions/Error'
                }
            }
        }
    },
},
{
    url: '/update/{id}',
    put: {
        summary: 'Update post',
        description: 'Create post details',
        parameters: [
            {
                in: 'path',
                name: 'id',
                description: 'postId',
                required: true,
                type: 'string'
            },
            {
                in: 'body',
                name: 'body',
                description: 'Model of post detail update',
                required: true,
                schema: {
                    $ref: '#/definitions/postUpdateReq'
                }
            }],
        responses: {
            default: {
                description: 'Unexpected error',
                schema: {
                    $ref: '#/definitions/Error'
                }
            }
        }
    },
}
]