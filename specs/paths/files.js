module.exports = [
    {
        url: '/',
        get: {
            summary: 'Search',
            description: 'get files lists',
            parameters: [
                {
                    in: 'query',
                    name: 'pageNo',
                    description: 'get  files',
                    required: false,
                    type: 'number'
                },
                {
                    in: 'query',
                    name: 'items',
                    description: 'get  files',
                    required: false,
                    type: 'number'
                },


            ],
            responses: {
                default: {
                    description: 'Unexpected error',
                    schema: {
                        $ref: '#/definitions/Error'
                    }
                }
            }
        },
        post: {
            summary: 'Upload file',
            description: 'Upload file',
            parameters: [{
                //     in: 'headers',
                //     name: 'x-access-token',
                //     description: 'token to access api',
                //     required: true,
                //     type: 'string'
                // }, {
                name: "file",
                in: "formData",
                description: "please choose an file",
                required: true,
                type: 'file'
            }, {
                in: 'body',
                name: "body",
                description: 'Model of document creation',
                required: true,
                schema: {
                    $ref: '#/definitions/fileCreateReq'
                }
            }],
            responses: {
                default: {
                    description: 'Unexpected error',
                    schema: {
                        $ref: '#/definitions/Error'
                    }
                }
            }
        }
    },
    {
        url: '/{id}',
        get: {
            summary: 'Get',
            description: 'get file by Id',
            parameters: [{
                //     in: 'header',
                //     name: 'x-access-token',
                //     description: 'token to access api',
                //     required: true,
                //     type: 'string'
                // },
                // {
                in: 'path',
                name: 'id',
                description: 'fileId',
                required: true,
                type: 'string'
            }
            ],
            responses: {
                default: {
                    description: 'Unexpected error',
                    schema: {
                        $ref: '#/definitions/Error'
                    }
                }
            }
        }
    },
    {
        url: '/delete/{id}',
        delete: {
            summary: 'delete',
            description: ' delete by Id',
            parameters: [
            {
                in: 'path',
                name: 'id',
                description: 'fileId',
                required: true,
                type: 'string'
            }
            ],
            responses: {
                default: {
                    description: 'Unexpected error',
                    schema: {
                        $ref: '#/definitions/Error'
                    }
                }
            }
        }
    }


]
