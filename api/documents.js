'use strict'

const response = require('../exchange/response')
const service = require('../services/documents')
const mapper = require('../mappers/document');
const api = require("./api-base")("documents","document")
const message = require("../helpers/message");


api.delete = async (req, res) => {
    const log = req.context.logger.start(`api/documents/delete/${req.params.id}`)
    try {
        const documents = await service.deletedocument(req.params.id, req.context)
        log.end()
        return response.data(res, 'successfully removed')
    } catch (err) {
        log.error(err.message)
        log.end()
        return response.failure(res, err.message)
    }
}

module.exports = api;