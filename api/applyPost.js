'use strict'

const response = require('../exchange/response')
const service = require('../services/applyPost')
const mapper = require('../mappers/applyPost')
const api = require("./api-base")("applyPost","applyPost")
module.exports = api;