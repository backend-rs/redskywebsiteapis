'use strict'

const response = require('../exchange/response');
const service = require('../services/posts');
const mapper = require('../mappers/post');
const api = require("./api-base")("posts","post")
const message = require("../helpers/message");

api.delete = async (req, res) => {
    const log = req.context.logger.start(`api/posts/delete/${req.params.id}`)
    try {
        const files = await service.deletepost(req.params.id, req.context)
        log.end()
        return response.data(res, 'successfully removed')
    } catch (err) {
        log.error(err.message)
        log.end()
        return response.failure(res, err.message)
    }
}
module.exports = api;
