'use strict'

const response = require('../exchange/response')
const service = require('../services/contactUs')
const mapper = require('../mappers/contactUs');
const api = require("./api-base")("contactUs","contactUs")
module.exports = api;