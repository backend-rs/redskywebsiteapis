'use strict'

const response = require('../exchange/response');
const service = require('../services/files');
const mapper = require('../mappers/file');
const api = require("./api-base")("files","file")
module.exports = api;