exports.toModel = entity =>{

    var model = {
        _id: entity._id,
        profile: entity.profile,
        technologies: entity.technologies,
        requirement: entity.requirement,
        skills:entity.skills,
        experience:entity.experience,
        jobType:entity.jobType,
        salary:entity.salary,
        schedule: entity.schedule,
        education: entity.education,
    }
    return model;
}

exports.toGetUser = entity =>{
    let model = entity;
    return model;
}