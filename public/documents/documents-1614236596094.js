'use strict'

const fs = require('fs')
const Jimp = require('jimp')
const path = require('path')
const appRoot = require('app-root-path')
const fileStoreConfig = require('config').get('providers.document-store')

const create = async (req, body, context) => {
    const log = context.logger.start('services/documents')
    try {
        let params = req.query
        let documentTemp

        let data = await upload(req.files.file)
             documentTemp = {
          
               
                url: data.url
            }
         
          
      

        let document = await new db.document(documentTemp).save()
        console.log(document)
        log.end()
        return document

    } catch (err) {
        log.end()
        throw new Error(err)
    }
}

const getById = async (id, context) => {
    const log = context.logger.start(`services/documents/get:${id}`)
    try {
        const document = await db.document.findById(id)
        log.end()
        return document
    } catch (err) {
        log.end()
        throw new Error(err)
    }
}







const get = async (req, page, context) => {
    const log = context.logger.start(`services/documents/get`)
    try {
        let document
        let params = req
        let query={}
      
        
            document = await db.document.find(query)
        // } else {
        //     document = await db.document.findAll({})
        // }

        log.end()
        return document
    } catch (err) {
        log.end()
        throw new Error(err)
    }
}


const deletedocument = async (id, context, res) => {
    const log = context.logger.start(`services/document/delete:${id}`)
    try {
        const document = await db.document.findByIdAndRemove(id)
        log.end()
        return document
    } catch (err) {
        log.end()
        throw new Error(err)
    }
}
// function to upload file
const upload = async (file, context) => {

    let parts = file.name.split('.')

    let name = parts[0]
    let ext = parts[1]

    let destDir = path.join(appRoot.path, fileStoreConfig.dir)

    let fileName = `${name}-${Date.now()}.${ext}`

    let destination = `${destDir}/${fileName}`
    let url = `${fileStoreConfig.root}/${fileName}`

    await move(file.path, destination)
    file.path = destination

    return {
        url: url

    }

}

const move = async (oldPath, newPath) => {
    const copy = (cb) => {
        var readStream = fs.createReadStream(oldPath)
        var writeStream = fs.createWriteStream(newPath)

        readStream.on('error', cb)
        writeStream.on('error', cb)

        readStream.on('close', function () {
            fs.unlink(oldPath, cb)
        })

        readStream.pipe(writeStream)
    }

    return new Promise((resolve, reject) => {
        fs.rename(oldPath, newPath, function (err) {
            if (err) {
                if (err.code === 'EXDEV') {
                    copy(err => {
                        if (err) {
                            reject(err)
                        } else {
                            resolve()
                        }
                    })
                } else {
                    reject(err)
                }
            } else {
                resolve()
            }
        })
    })
}

exports.upload = upload
exports.create = create

exports.get = get
exports.getById=getById
exports.deletedocument=deletedocument

































// const resizeImage = async (file, context) => {
//     let parts = file.name.split('.')

//     let name = parts[0]
//     let ext = parts[1]

//     let destDir = path.join(appRoot.path, fileStoreConfig.dir)

//     let fileName = `${name}-${Date.now()}.${ext}`

//     let destination = `${destDir}/${fileName}`
//     let url = `${fileStoreConfig.root}/${fileName}`

//     // await Jimp.read(file.path)
//     //     .then(lenna => {
//     //         return lenna
//     //             .resize(256, 256) // resize
//     //             .quality(60) // set JPEG quality
//     //             .write(path.join(destination)) // save
//     //     })
//     //     .catch(err => {
//     //         throw new Error(err)
//     //     })

//     // const thumbnail = await imagethumbnail(destination)

//     return {
//         url,
//         destination
//         // thumbnail
//     }
// }

// const imagethumbnail = (path) => {
//     if (!path) {
//         return Promise.resolve(null)
//     }

//     return new Promise((resolve, reject) => {
//         return Jimp.read(path).then(function (lenna) {
//             if (!lenna) {
//                 return resolve(null)
//             }
//             var a = lenna.resize(15, 15) // resize
//                 .quality(50) // set JPEG quality
//                 .getBase64(Jimp.MIME_JPEG, function (result, base64, src) {
//                     return resolve(base64).save()
//                 })
//         }).catch(function (err) {
//             reject(err)
//         })
//     })
// }