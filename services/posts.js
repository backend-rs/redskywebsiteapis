'use strict'



const encrypt = require("../permit/crypto");
const auth = require("../permit/auth");
const randomize = require("randomatic");
const nodemailer = require("nodemailer");
const response = require("../exchange/response");
const message = require("../helpers/message");
const helpers = require("../helpers/mail-html");
const otp = require("../helpers/otp");
const moment = require("moment");

const set=(model,entity,context)=>{
    const log = context.logger.start('services/posts/set')
    if(!model.profile == 'string'){
        entity.profile = model.profile
    }
    if(!model.technologies == 'string'){
        entity.technologies = model.technologies
    }
    if(!model.requirement == 'string'){
        entity.requirement = model.requirement
    }
    if(!model.skills == 'string'){
        entity.skills  = model.skills
    }
    if(model.experience){
        entity.experience = model.experience
    }
    if(model.jobType){
        entity.jobType = model.jobType
    }
    if(model.salary){
        entity.salary = model.salary
    }
    if(model.schedule){
        entity.schedule = model.schedule
    }
    if(model.education){
        entity.education = model.education
    }
    if(model.updatedAt){
        entity.updatedAt = Date.now()
    }
    if(model.updatedby){
        entity.updatedby = Date.now()
    }
    log.end()
    entity.save()
    return entity
}



const create = async (req,model, context,res) => {
    model.createdAt = Date.now()
    const log = context.logger.start('services/posts')
    try {
        let post = await new db.post(model).save()
        
        return post
    } catch (err) {
        log.end()
        throw new Error(err)
    }
}


const get = async (req, page, context) => {
    const log = context.logger.start(`services/posts/get`)
    let post
    let queryModel
    const params = req.query;
    let pageNo = Number(params.pageNo) || 1
    let items = Number(params.items) || 10
    let skipCount = items * (pageNo - 1)
    try {
        if ( (pageNo != undefined || pageNo != null) && (items != undefined || items != null)){
           
             post = await db.post.find(queryModel).skip(skipCount).limit(items).sort({
                timeStamp: -1
            });
            
        } else {
              post = await db.post.find()
    
        }
        log.end()
        return post

    } catch (err) {
        log.end()
        throw new Error(err)
    }
}
const getById = async (id, context) => {
    const log = context.logger.start(`services/posts/get:${id}`)
    try {
        const post = await db.post.findById(id)
        log.end()
        return post
    } catch (err) {
        log.end()
        throw new Error(err)
    }
}
const update = async (id, model, context) => {
    const log = context.logger.start(`services/posts/update:${id}`)
    try {
        const entity = await db.post.findById(id)

        if (!entity) {
            throw new Error('invalid Id')
        }
        await set(model, entity, context);

        return entity
    } catch (err) {
        log.end()
        throw new Error(err)
    }
}
const deletepost = async (id, context) => {
    const log = context.logger.start(`services/posts/delete:${id}`)
    try {
        const post = await db.post.findByIdAndRemove(id)
        log.end()
        return post
    } catch (err) {
        log.end()
        throw new Error(err)
    }
}
exports.create = create
exports.get = get
exports.getById = getById
exports.deletepost = deletepost
exports.update = update