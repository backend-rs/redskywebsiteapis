'use strict'
const nodemailer = require('nodemailer');
const service = require('../services/documents')

// send mail function
const sendMail = async (email, transporter, subject,  html) => {
    const details = {
        from: 'nodejs.redsky@gmail.com',
        to: email,
        subject: subject,
        html: html,
        // attachments: [{
        //     filename: text.name,
        //     path: text.path,
        //     contentType: 'application/pdf'
        // }]
    }
    var info = await transporter.sendMail(details)
    console.log("INFO:::", info)
}

const create = async (req, model, context) => {
    
    const log = context.logger.start('services/applyPost')
    try {
        let applyPost
        //let data = await service.upload(req.files.file)
      
        let postModel = {
            firstName: model.firstName,
            lastName: model.lastName,
            email: model.email,
            phone: model.phone,
            qualification: model.qualification,
            college: model.college,
            post:data.url 
        }
        applyPost = await new db.applyPost(postModel).save()
        console.log(applyPost)

        // transporter
        var transporter = nodemailer.createTransport({
            service: 'gmail',
            auth: {
                user: 'nodejs.redsky@gmail.com',
                pass: '@njred*132000'
            }
        })

        const subject = "contact"
        // const text = "details"

        const contactDetails = "<b>First name:</b>" + "  " + applyPost.firstName + "<br>" +
            "<b>Last name:</b>" + "  " + applyPost.lastName + "<br>" +
            "<b>Email:</b>" + "  " + applyPost.email + "<br>" +
            "<b>Phone:</b>" + "  " + applyPost.phone + "<br>" +
            "<b>Qualification:</b>" + "  " + applyPost.qualification + "<br>"+
            "<b>College/University:</b>" + "  " + applyPost.college + "<br>"
            //"<b>Resume:</b>" + "  " + applyPost.pdf + "<br>"
        // call sendMail method
        await sendMail('info@redskyatech.com', transporter, subject,  contactDetails)

        log.end()
        return 'Mail send successfully'

    } catch (err) {
        log.end()
        throw new Error(err)
    }
}
exports.create = create