'use strict'

const fs = require('fs')
const Jimp = require('jimp')
const path = require('path')
const appRoot = require('app-root-path')
const fileStoreConfig = require('config').get('providers.document-store')

const create = async (req, body, context,res) => {
    const log = context.logger.start('services/documents')
    try {
        let documentTemp
        let data = await upload(req.files.file)
        
        documentTemp = {
            url: data.url
        }
        let document = await new db.documents(documentTemp).save()
       
        log.end()
        return document

    } catch (err) {
        log.end()
        throw new Error(err)
    }
}

const getById = async (id, context) => {
    const log = context.logger.start(`services/documents/get:${id}`)
    try {
        const document = await db.documents.findById(id)
        log.end()
        return document
    } catch (err) {
        log.end()
        throw new Error(err)
    }
}

const get = async (req, page, context) => {
    const log = context.logger.start(`services/documents/get`)
    let documents
    let queryModel
    const params = req.query;
    let pageNo = Number(params.pageNo) || 1
    let items = Number(params.items) || 10
    let skipCount = items * (pageNo - 1)
    try {
        if ( (pageNo != undefined || pageNo != null) && (items != undefined || items != null)){
           
            // find files
            documents = await db.documents.find(queryModel).skip(skipCount).limit(items).sort({
                timeStamp: -1
            });
           
            
        } else {
            documents = await db.documents.find()
    
        }
        log.end()
        return documents

    } catch (err) {
        log.end()
        throw new Error(err)
    }
}


const deletedocument = async (id, context, res) => {
    const log = context.logger.start(`services/documents/delete:${id}`)
    try {
        const document = await db.documents.findByIdAndRemove(id)
        log.end()
        return document
    } catch (err) {
        log.end()
        throw new Error(err)
    }
}
// function to upload file
const upload = async (file, context) => {

    let parts = file.name.split('.')

    let name = parts[0]
    let ext = parts[1]

    let destDir = path.join(appRoot.path, fileStoreConfig.dir)

    let fileName = `${name}-${Date.now()}.${ext}`

    let destination = `${destDir}/${fileName}`
    let url = `${fileStoreConfig.root}/${fileName}`

    await move(file.path, destination)
    file.path = destination

    return {
        url: url
    }

}

const move = async (oldPath, newPath) => {
    const copy = (cb) => {
        var readStream = fs.createReadStream(oldPath)
        var writeStream = fs.createWriteStream(newPath)

        readStream.on('error', cb)
        writeStream.on('error', cb)

        readStream.on('close', function () {
            fs.unlink(oldPath, cb)
        })

        readStream.pipe(writeStream)
    }

    return new Promise((resolve, reject) => {
        fs.rename(oldPath, newPath, function (err) {
            if (err) {
                if (err.code === 'EXDEV') {
                    copy(err => {
                        if (err) {
                            reject(err)
                        } else {
                            resolve()
                        }
                    })
                } else {
                    reject(err)
                }
            } else {
                resolve()
            }
        })
    })
}

exports.upload = upload
exports.create = create

exports.get = get
exports.getById = getById
exports.deletedocument = deletedocument

































