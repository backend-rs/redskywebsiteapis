'use strict'

const response = require('../exchange/response')

exports.canCreate = (req, res, next) => {
    if (!req.body.postTitle) {
        response.failure(res, 'postTitle is required')
    }
    if (!req.body.postType) {
        response.failure(res, 'postType is required')
    }
    if (!req.body.profile) {
        response.failure(res, 'Profile is required')
    }
    if (!req.body.Responsibilities) {
        response.failure(res, 'Responsibilities is required')
    }

    if (!req.body.technologies) {
        response.failure(res, 'technologies is required')
    }
    if (!req.body.requirement) {
        response.failure(res, 'requirement is required')
    }
    if (!req.body.skills) {
        response.failure(res, 'skills is required')
    }
    if (!req.body.experience) {
        response.failure(res, 'experience is required')
    }
    if (!req.body.jobType) {
        response.failure(res, 'jobType is required')
    }
    if (!req.body.salary) {
        response.failure(res, 'salary is required')
    }
    if (!req.body.schedule) {
        response.failure(res, 'schedule is required')
    }
    if (!req.body.education) {
        response.failure(res, 'education is required')
    }
    if (!req.body.WorkRemotely) {
        response.failure(res, 'WorkRemotely is required')
    }
    return next()

}
exports.getById = (req, res, next) => {

    if (!req.params && !req.params.id) {

        return response.failure(res, 'id is required')
    }

    return next()
}


exports.update = (req, res, next) => {
    if (!req.params && !req.params.id) {
        return response.failure(res, 'id is required')
    }
    return next()
}

exports.login = (req, res, next) => {
    if (!req.body.email) {
        response.failure(res, 'email is required')
    }
    if (!req.body.password) {
        response.failure(res, 'Password  is required')
    }
    return next()
}

exports.changePassword = (req, res, next) => {
    if (!req.body.password) {
        response.failure(res, 'Password is required')
    }
    if (!req.body.newPassword) {
        response.failure(res, 'NewPassword is required')
    }

    return next()
}
exports.forgotPassword = (req, res, next) => {
    if (!req.body.email) {
        response.failure(res, 'email is required')
    }
    return next()
}

exports.resetPassword = (req, res, next) => {
    if (!req.body.otp) {
        response.failure(res, 'otp is required')
    }
    if (!req.body.newPassword) {
        response.failure(res, 'new password is required')
    }
    return next()
}