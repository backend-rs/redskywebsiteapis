'use strict'

const fs = require('fs')
const specs = require('../specs')
const api = require('../api')
var auth = require('../permit')
const validator = require('../validators')
const fileUpload = require('express-fileupload');
const bodyParser = require('body-parser');


const multipart = require('connect-multiparty')
const multipartMiddleware = multipart()

const configure = (app, logger) => {
    const log = logger.start('settings:routes:configure')

    app.get('/specs', function (req, res) {
        fs.readFile('./public/specs.html', function (err, data) {
            if (err) {
                return res.json({
                    isSuccess: false,
                    error: err.toString()
                })
            }
            res.contentType('text/html')
            res.send(data)
        })
    })

    app.get('/api/specs', function (req, res) {
        res.contentType('application/json')
        res.send(specs.get())
    })

    // .......................................contactUs routes.......................................
    app.post('/api/contactUs', auth.context.builder, validator.contactUs.canCreate, api.contactUs.create);


    // ......................................applyPost routes..........................................
    app.post('/api/applyPost', auth.context.builder,multipartMiddleware, validator.applyPost.canCreate, api.applyPost.create);

    //.......................................post routes.............................................
    app.post('/api/posts',auth.context.builder,validator.post.canCreate,api.post.create);
    app.get('/api/posts',auth.context.builder,api.post.get);
    app.get('/api/posts/:id',auth.context.builder,api.post.getById);
    app.put('/api/posts/update/:id', auth.context.builder,api.post.update);
    app.delete('/api/posts/delete/:id', auth.context.builder, api.post.delete);
  
    //................................upload documents.........................................
    // app.post('/api/documents/upload', multipartMiddleware, api.documents.upload);
     app.post('/api/documents',auth.context.builder,multipartMiddleware,api.documents.create);
     app.get('/api/documents/:id', auth.context.builder, api.documents.getById);
     app.get('/api/documents', auth.context.builder, api.documents.get);
     app.delete('/api/documents/delete/:id', auth.context.builder, api.documents.delete);
    //................................upload files............................................

    // app.post('/api/files', auth.context.builder, multipartMiddleware, api.files.create);
    // app.post('/api/files/upload', multipartMiddleware, api.files.upload);
    // app.get('/api/files/:id', auth.context.builder, api.files.getById);
    // app.get('/api/files', auth.context.builder, api.files.get);
    // app.delete('/api/files/delete/:id', auth.context.builder, api.files.delete);
    log.end()
}
exports.configure = configure